import {QuestionSource} from "./QuestionSource.js"
import * as fs from 'fs';
import {shuffleArray} from "../util.js";
import {OpenAI} from "openai";


export const QuizDBQuestionSource = new QuestionSource(
    "MemoryDB",
    (sourceObj) => {
        let rawdata = fs.readFileSync('./src/QuestionData/quiz-db.txt');
        let rawquestions = JSON.parse(rawdata);
        let shuffledrawquestions = shuffleArray(rawquestions);
        var questions = []
        for (let q of shuffledrawquestions) {
            questions.push({
                question: q.question,
                answers: [q.A, q.B, q.C, q.D],
                correctId: q.answer.charCodeAt(0) - 'A'.charCodeAt(0)
            })
        }

        sourceObj.sourcelist = questions;
    },
    async (callId, sourceObj, data) => {
        let randid = Math.floor(Math.random() * sourceObj.sourcelist.length)
        const questionObj = sourceObj.sourcelist[randid]
        sourceObj.setFuture(
            callId,
            questionObj.question,
            questionObj.answers,
            questionObj.correctId
        )
    }
);

export const GPT3QuestionSource = new QuestionSource(
    "AIService",
    (sourceObj) => {

        sourceObj.openai = new OpenAI(
            fs.readFileSync('./src/QuestionData/apikey.txt').toString(),
            fs.readFileSync('./src/QuestionData/orgkey.txt').toString()
        );
        sourceObj.questionPrompt = fs.readFileSync('./src/QuestionData/QuestionPrompt.txt').toString();
        sourceObj.answerPrompt = fs.readFileSync('./src/QuestionData/AnswerPrompt.txt').toString();
    },
    async (callId, sourceObj, data) => {
        data.topic = data.topic.substr(0, 32)
        let topicelements = data.topic.split(" ")
        if (topicelements.length >= 2) {
            var topic = topicelements[0] + " " + topicelements[1]
        } else {
            var topic = topicelements[0]
        }
        let questionCompl = await sourceObj.openai.complete("davinci", {
            prompt: sourceObj.questionPrompt + topic.toLocaleLowerCase() + "\nQuestion:",
            stop: "\n",
            max_tokens: 64,
            temperature: 0.4,
            top_p: 1,
            n: 1,
        })

        let answerCompl = await sourceObj.openai.complete("davinci", {
            prompt: sourceObj.answerPrompt + questionCompl.choices[0].text + "\nAnswers:",
            stop: "\n",
            max_tokens: 64,
            temperature: 0.05,
            top_p: 1,
            frequency_penalty: 0.4,
            presence_penalty: 0.3,
            n: 1,
        })

        let questionObj = {
            question: questionCompl.choices[0].text.substr(
                1,
                questionCompl.choices[0].text.length - 1
            ),
            answers: answerCompl.choices[0].text.substr(
                1,
                answerCompl.choices[0].text.length - 1
            ).split("; "),
            correctId: 0
        }
        sourceObj.setFuture(
            callId,
            questionObj.question,
            questionObj.answers,
            questionObj.correctId
        )
        console.log("Future set")
    }
);

/*
exports.QuizDBQuestionSource = QuizDBQuestionSource
exports.GPT3QuestionSource = GPT3QuestionSource
*/
