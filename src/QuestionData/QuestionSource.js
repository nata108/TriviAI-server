import {
    performance
} from 'perf_hooks';

export class QuestionSource {
    constructor(type, initfunc, asyncfunction) {
        this.futures = {}
        this.generate = asyncfunction
        this.initfunc = initfunc
        this.initfunc(this)
        this.type = type
    }

    setFuture(callId, question, answers, correctId) {
        this.futures[callId].setData(question, answers, correctId)
        delete this.futures[callId]
    }

    generateQuestion(data) {
        var future = new QuestionFuture()
        let callId = performance.now()
        this.futures[callId] = future
        this.generate(callId, this, data)
        return future
    }
}

class QuestionFuture {
    constructor() {
        this.ready = false
    }

    setData(question, answers, correctId) {
        this.question = question;
        this.answers = answers;
        this.correctId = correctId;
        this.ready = true
    }

    getData() {
        if (this.ready) {
            return {
                question: this.question,
                answers: this.answers,
                correctId: this.correctId
            }
        } else {
            return null
        }
    }

}

//exports.QuestionSource = QuestionSource
