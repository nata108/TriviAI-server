import {EventHandler} from "./EventHandler.js"
import {Engine} from "../GameEngine/TriviAIEngine.js"


const STATE = {
    ERROR: 0,
    SUCCESSFUL: 1
}

const getRoomInfo = (uuid, successMessage, retIDs = false) => {
    var [roomsuccess, roominfo] = Engine.getRoomInfo(uuid)
    if (roomsuccess) {
        var stateObj = {state: STATE.SUCCESSFUL, message: successMessage};
        let adminName = Engine.getName(roominfo.admin);
        let usernames = []
        for (let id of roominfo.users) {
            usernames.push(Engine.getName(id));
        }
        var roomInfoObj = {
            roomCode: roominfo.code,
            userNames: usernames,
            currentSettings: roominfo.settings,
            adminName: adminName
        };
        var userIDs = roominfo.users
    } else {
        var stateObj = {state: STATE.ERROR, message: "User is not in a valid room."};
        var roomInfoObj = null;
        var userIDs = []
    }
    if (retIDs) {
        return [stateObj, roomInfoObj, userIDs];
    } else {
        return [stateObj, roomInfoObj];
    }
}

const getRoomInfoByCode = (roomcode, successMessage, retIDs = false) => {
    var [roomsuccess, roominfo] = Engine.getRoomInfoByCode(roomcode)
    if (roomsuccess) {
        var stateObj = {state: STATE.SUCCESSFUL, message: successMessage};
        let adminName = Engine.getName(roominfo.admin);
        let usernames = []
        for (let id of roominfo.users) {
            usernames.push(Engine.getName(id));
        }
        var roomInfoObj = {
            roomCode: roominfo.code,
            userNames: usernames,
            currentSettings: roominfo.settings,
            adminName: adminName
        };
        var userIDs = roominfo.users
    } else {
        var stateObj = {state: STATE.ERROR, message: "User is not in a valid room."};
        var roomInfoObj = null;
        var userIDs = [];
    }
    if (retIDs) {
        return [stateObj, roomInfoObj, userIDs];
    } else {
        return [stateObj, roomInfoObj];
    }
}


export const RegisterClientEventHandler = new EventHandler(
    "RegisterClient",
    (client, data, allClients) => {
        if (Engine.addUser(allClients.get(client), data.name)) {
            var stateObj = {state: STATE.SUCCESSFUL, message: "User registered."};
        } else {
            var stateObj = {state: STATE.ERROR, message: "Name already taken, or contains invalid sequences!"};
        }
        const response = {type: "RegisterClientResponse", name:data.name, stateObj: stateObj};
        client.send(JSON.stringify(response));
    }
);

export const CreateRoomEventHandler = new EventHandler(
    "CreateRoom",
    (client, data, allClients) => {
        if (!Engine.checkId(allClients.get(client))) {
            const response = {
                type: "RegisterClientResponse",
                stateObj: {state: STATE.ERROR, message: "User not found."},
                roomInfoObj: null
            };
            client.send(JSON.stringify(response));
            return
        }

        let [success, msg] = Engine.createRoom(allClients.get(client));
        if (success) {
            [stateObj, roomInfoObj] = getRoomInfo(allClients.get(client), "Room created.")
        } else {
            var stateObj = {state: STATE.ERROR, message: msg};
            var roomInfoObj = null;
        }

        const response = {
            type: "RoomInfoUpdate",
            stateObj: stateObj,
            roomInfoObj: roomInfoObj
        };
        client.send(JSON.stringify(response));
    }
);

export const LeaveRoomEventHandler = new EventHandler(
    "LeaveRoom",
    (client, data, allClients) => {
        if (!Engine.checkId(allClients.get(client))) {
            const response = {
                type: "RegisterClientResponse",
                stateObj: {state: STATE.ERROR, message: "User not found."},
            };
            client.send(JSON.stringify(response));
            return
        } else {
            var roomcode = Engine.getUserRoom(allClients.get(client));
            let [success, msg] = Engine.leaveRoom(allClients.get(client));
            if (success) {
                let [stateObj, roomInfoObj, userIDs] = getRoomInfoByCode(
                    roomcode,
                    "Player left.",
                    true
                )
                const broadcastResponse = {
                    type: "RoomInfoUpdate",
                    stateObj: stateObj,
                    roomInfoObj: roomInfoObj
                };
                for (let c of allClients.keys()) {
                    if (userIDs.includes(allClients.get(c))) {
                        c.send(JSON.stringify(broadcastResponse));
                    }
                }
                var state = STATE.SUCCESSFUL
            } else {
                var state = STATE.ERROR
            }
            const response = {
                type: "RegisterClientResponse",
                stateObj: {state: state, message: msg},
            };
            client.send(JSON.stringify(response));
        }
    }
);

export const JoinRoomEventHandler = new EventHandler(
    "JoinRoom",
    (client, data, allClients) => {
        if (!Engine.checkId(allClients.get(client))) {
            const response = {
                type: "RegisterClientResponse",
                stateObj: {state: STATE.ERROR, message: "User not found."},
            };
            client.send(JSON.stringify(response));
            return
        }

        let [success, msg] = Engine.joinRoom(allClients.get(client), data.roomCode)
        if (success) {
            var [stateObjBroadcast, roomInfoObjBroadcast, userIDs] = getRoomInfoByCode(
                data.roomCode,
                "Player joined.",
                true
            )
            const broadcastResponse = {
                type: "RoomInfoUpdate",
                stateObj: stateObjBroadcast,
                roomInfoObj: roomInfoObjBroadcast
            };
            //console.log(userIDs)
            for (let c of allClients.keys()) {
                //console.log(allClients.get(c))
                if (userIDs.includes(allClients.get(c))) {
                    //console.log("sending")
                    c.send(JSON.stringify(broadcastResponse));
                }
            }
        } else {
            var stateObj = {state: STATE.ERROR, message: msg};
            var roomInfoObj = null;
            const response = {
                type: "RoomInfoUpdate",
                stateObj: stateObj,
                roomInfoObj: roomInfoObj
            };
            client.send(JSON.stringify(response));
        }

    }
);

export const UpdateRoomSettingsEventHandler = new EventHandler(
    "UpdateRoomSettings",
    (client, data, allClients) => {
        if (!Engine.checkId(allClients.get(client))) {
            const response = {
                type: "RoomInfoUpdate",
                stateObj: {state: STATE.ERROR, message: "User not found."},
            };
            client.send(JSON.stringify(response));
            return
        }
        let [success, msg] = Engine.changeRoomSettings(
            allClients.get(client),
            data.settingsObj
        );
        if (success) {
            var [stateObjBroadcast, roomInfoObjBroadcast, userIDs] = getRoomInfo(
                allClients.get(client),
                "Settings updated.",
                true
            )
            const broadcastResponse = {
                type: "RoomInfoUpdate",
                stateObj: stateObjBroadcast,
                roomInfoObj: roomInfoObjBroadcast
            };
            //console.log(userIDs)
            for (let c of allClients.keys()) {
                //console.log(allClients.get(c))
                if (userIDs.includes(allClients.get(c))) {
                    //console.log("sending")
                    c.send(JSON.stringify(broadcastResponse));
                }
            }
        } else {
            var stateObj = {state: STATE.ERROR, message: msg};
            var roomInfoObj = null;
            const response = {
                type: "RoomInfoUpdate",
                stateObj: stateObj,
                roomInfoObj: roomInfoObj
            };
            client.send(JSON.stringify(response));
        }
    }
);

export const StartGameEventHandler = new EventHandler(
    "StartGame",
    (client, data, allClients) => {
        if (!Engine.checkId(allClients.get(client))) {
            const response = {
                type: "RegisterClientResponse",
                stateObj: {state: STATE.ERROR, message: "User not found."},
            };
            client.send(JSON.stringify(response));
            return
        }
        let [success, msg] = Engine.startGame(allClients.get(client));
        if (!success) {
            var stateObj = {state: STATE.ERROR, message: msg};
            var roomInfoObj = null;
            const response = {
                type: "RoomInfoUpdate",
                stateObj: stateObj,
                roomInfoObj: roomInfoObj
            };
            client.send(JSON.stringify(response));
        }

    }
);

export const GameInfoUpdateRequestEventHandler = new EventHandler(
    "GameInfoUpdateRequest",
    (client, data, allClients) => {
        if (client != null) {
            if (!Engine.checkId(allClients.get(client))) {
                const response = {
                    type: "RegisterClientResponse",
                    stateObj: {state: STATE.ERROR, message: "User not found."},
                };
                client.send(JSON.stringify(response));
                return
            }
            let [success, gamedata] = Engine.getLastGameInfo(allClients.get(client))
            if (success) {
                var state = STATE.SUCCESSFUL
                var message = "Client requested update."
                var gameInfoObj = gamedata
            } else {
                var state = STATE.ERROR
                var message = gamedata
                var gameInfoObj = null
            }
            const response = {
                type: "GameInfoUpdate",
                stateObj: {state: state, message: message},
                gameInfoObj: gameInfoObj
            }
            client.send(JSON.stringify(response));

        } else {
            let [success, userIDs] = Engine.getUsersByGameId(data.roomCode)
            //console.log([success, userIDs])
            if (success) {
                let broadcastResponse = {
                    type: "GameInfoUpdate",
                    stateObj: {
                        state: STATE.SUCCESSFUL,
                        message: "Server update"
                    },
                    gameInfoObj: data
                }
                for (let c of allClients.keys()) {
                    //console.log(allClients.get(c))
                    if (userIDs.includes(allClients.get(c))) {
                        //console.log("sending")
                        c.send(JSON.stringify(broadcastResponse));
                    }
                }
                return true;
            } else {
                return false;
            }
        }
    }
);

export const SendTopicEventHandler = new EventHandler(
    "SendTopic",
    (client, data, allClients) => {
        if (!Engine.checkId(allClients.get(client))) {
            const response = {
                type: "RegisterClientResponse",
                stateObj: {state: STATE.ERROR, message: "User not found."},
            };
            client.send(JSON.stringify(response));
            return
        }
        let [success, respdata] = Engine.saveTopics(allClients.get(client), data.topics)
        if (success) {
            var state = STATE.SUCCESSFUL
            var message = "Client requested update."
            var gameInfoObj = respdata
        } else {
            var state = STATE.ERROR
            var message = respdata
            var gameInfoObj = null
        }
        const response = {
            type: "GameInfoUpdate",
            stateObj: {state: state, message: message},
            gameInfoObj: gameInfoObj
        }
        client.send(JSON.stringify(response));

    }
);


export const SendAnswerEventHandler = new EventHandler(
    "SendAnswer",
    (client, data, allClients) => {
        if (!Engine.checkId(allClients.get(client))) {
            const response = {
                type: "RegisterClientResponse",
                stateObj: {state: STATE.ERROR, message: "User not found."},
            };
            client.send(JSON.stringify(response));
            return
        }
        let [success, respdata] = Engine.saveAnswer(allClients.get(client), data.answerId)
        if (success) {
            var state = STATE.SUCCESSFUL
            var message = "Client requested update."
            var gameInfoObj = respdata
        } else {
            var state = STATE.ERROR
            var message = respdata
            var gameInfoObj = null
        }
        const response = {
            type: "GameInfoUpdate",
            stateObj: {state: state, message: message},
            gameInfoObj: gameInfoObj
        }
        client.send(JSON.stringify(response));

    }
);

export const PersonalGameInfoUpdateRequestEventHandler = new EventHandler(
    "PersonalGameInfoUpdateRequest",
    (client, data, allClients) => {
        let broadcastResponse = {
            type: "GameInfoUpdate",
            stateObj: {
                state: STATE.SUCCESSFUL,
                message: "Server update"
            },
            gameInfoObj: data.response
        }
        for (let c of allClients.keys()) {
            //console.log(allClients.get(c))
            if (data.clients.includes(allClients.get(c))) {
                //console.log("sending")
                c.send(JSON.stringify(broadcastResponse));
            }
        }
        return true;
    }
);

export const ConnectionLostEventHandler = new EventHandler(
    "ConnectionLost",
    (client, data, allClients) => {
        if (Engine.checkId(allClients.get(client))) {
            Engine.leaveRoom(allClients.get(client))
            Engine.removeUser(allClients.get(client))
        }
    }
)

/*
exports.RegisterClientEventHandler = RegisterClient
exports.CreateRoomEventHandler = CreateRoom
exports.LeaveRoomEventHandler = LeaveRoom
exports.JoinRoomEventHandler = JoinRoom
exports.UpdateRoomSettingsEventHandler = UpdateRoomSettings
exports.StartGameEventHandler = StartGame
exports.GameInfoUpdateRequestEventHandler = GameInfoUpdateRequest
exports.SendTopicEventHandler = SendTopic
exports.SendAnswerEventHandler = SendAnswer
exports.PersonalGameInfoUpdateRequestEventHandler = PersonalGameInfoUpdateRequest
exports.ConnectionLostEventHandler = ConnectionLost
*/
