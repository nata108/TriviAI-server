import {WebSocketServer} from 'ws'
import {uuid} from 'uuidv4';

export class EventReactor {
    constructor(ip, port) {
        this.handlers = {};
        this.events = []
        this.notifyLoop = null

        this.wss = new WebSocketServer({ip: ip, port: port});
        this.clients = new Map();

        this.wss.on('connection', (ws) => {
            const id = uuid();
            this.clients.set(ws, id);
            console.log(id + " connected");

            ws.on("message", (messageString) => {
                try {
                    let message = JSON.parse(String(messageString))
                    this.dispatch(ws, message.type, message);
                }
                catch (e) {

                }
            })

            ws.on("close", () => {
                console.log(this.clients.get(ws) + " disconnected")
                this.dispatch(ws, "ConnectionLost", null);
                this.clients.delete(ws)
            })
        });
    }

    registerHandler(newHandler) {
        if (this.handlers[newHandler.eventName] != null) {
            this.handlers[newHandler.eventName].push(newHandler);
        } else {
            this.handlers[newHandler.eventName] = [newHandler,];
        }
    }

    dispatch(client, type, data) {
        if (type in this.handlers) {
            for (var handler of this.handlers[type]) {
                handler.handle(client, data, this.clients);
            }
        }
    }

    dispatchManually(type, data) {
        if (type in this.handlers) {
            for (var handler of this.handlers[type]) {
                handler.handle(null, data, this.clients);
            }
        }
    }


}

/*
exports.EventReactor = EventReactor
*/
