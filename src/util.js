export const shuffleArray = (unshuffled) => {
    return unshuffled
        .map((value) => ({value, sort: Math.random()}))
        .sort((a, b) => a.sort - b.sort)
        .map(({value}) => value)
}

export const sleep = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
}

/*
exports.shuffleArray = shuffleArray
exports.sleep = sleep
*/
