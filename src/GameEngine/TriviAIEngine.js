import {shuffleArray} from "../util.js"
import {QuestionManager} from "./QuestionManager.js"
import {QuizDBQuestionSource, GPT3QuestionSource} from "../QuestionData/Sources.js"

const GAMESTATE = {
    GAME_START: 0,
    TOPIC: 1,
    WAIT: 2,
    ROUND_START: 3,
    QUESTION: 4,
    GAME_END: 5
}

const ROUNDTYPE = {
    REGULAR: 0,
    WILDCARD: 1
}

const WAITTIME = 5000;
const OVERHEADPADDINGTIME = 500;

class TriviAIEngine {
    constructor() {
        this.rooms = {};
        this.users = {};
        this.games = {};
    }

    createRoom(uuid) {
        if (this.users[uuid].room == null) {
            var code = Math.random().toString(36).substring(2, 7).toUpperCase();
            while (Object.keys(this.rooms).some(roomcode => {
                return roomcode == code
            })) {
                code = Math.random().toString(36).substring(2, 7).toUpperCase();
            }

            const settingsObj = {
                regularRounds: 5,
                wildcardRounds: 2,
                roundLength: 15,
                wildcardScoreMultiplier: 1.0,
                relativeScoring: false
            }
            this.rooms[code] = {admin: uuid, users: [uuid,], settings: settingsObj}
            this.rooms[code].destroyer = setTimeout(this.destroyRoom.bind(this), 300000, code)
            this.users[uuid].room = code;

            return [true, code];
        } else {
            if (Object.keys(this.games).some(gameid => {
                return gameid == this.users[uuid].room
            })) {
                let gameid = this.users[uuid].room
                if (Object.keys(this.rooms).some(roomcode => {
                    return roomcode == gameid
                })) {
                    return [false, "A room already exists with this code!"]
                }
                if (this.games[gameid].admin != uuid) {
                    return [false, "Only the admin can recreate this game."]
                }
                this.rooms[gameid] = {
                    admin: uuid,
                    users: [uuid,],
                    settings: this.games[gameid].settings
                }
                this.games[gameid].users = this.games[gameid].users.filter((id) => {
                    return id != uuid
                })
                let gameObj = this.games[gameid].lastGameObject
                gameObj.rejoinable = true
                this.reactor.dispatchManually("GameInfoUpdateRequest", gameObj)
                return [true, gameid]
            }
            return [false, "User is in room and cannot create a new one."];
        }

    };

    checkId(uuid) {
        return Object.keys(this.users).some(id => {
            return id == uuid
        });
    }

    getName(uuid) {
        if(!Object.keys(this.users).includes(uuid)){
            return "Disconnected User"
        }
        return this.users[uuid].name;
    }

    createLeaderBoard(g) {
        let leaderBoard = []
        for (let data of g.scores) {
            leaderBoard.push({name: this.getName(data.uuid), score: data.score})
        }
        return leaderBoard
    }

    getUserRoom(uuid) {
        return this.users[uuid].room;
    }

    getRoomInfo(uuid) {
        let roomcode = this.users[uuid].room;
        if (roomcode != null) {
            if (!Object.keys(this.rooms).some(code => {
                return code == roomcode
            })) {
                return [false, null]
            }
            let roominfo = this.rooms[roomcode];
            roominfo.code = roomcode;
            return [true, roominfo];
        } else {
            return [false, null];
        }
    }

    getRoomInfoByCode(roomcode) {
        if (roomcode != null) {
            if (!Object.keys(this.rooms).some(code => {
                return code == roomcode
            })) {
                return [false, null]
            }
            let roominfo = this.rooms[roomcode];
            roominfo.code = roomcode;
            return [true, roominfo];
        } else {
            return [false, null];
        }
    }

    addUser(uuid, name) {
        if(name==="" || name.includes("👑") || name.includes("(You)") || name.includes("🥇") || name.includes("🥈") || name.includes("🥉")){
            return false;
        }
        if (!Object.values(this.users).some(data => {
            return data.name == name
        })) {
            if (Object.keys(this.users).some(data => {
                return data == uuid
            })) {
                this.users[uuid].name = name;
            } else {
                this.users[uuid] = {name: name, room: null}
            }
            return true;
        } else {
            return false;
        }
    }

    addReactor(reactor) {
        this.reactor = reactor;

    }

    leaveRoom(uuid) {
        if (this.users[uuid].room == null) {
            return [true, "User is not in any room."]
        }

        let roomcode = this.users[uuid].room
        if (Object.keys(this.rooms).includes(roomcode)) {
            this.rooms[roomcode].users = this.rooms[roomcode].users.filter((id) => {
                return id != uuid
            })
            this.users[uuid].room = null
            if (this.rooms[roomcode].users.length <= 0) {
                clearTimeout(this.rooms[roomcode].destroyer)
                delete this.rooms[roomcode]
            } else {
                if (this.rooms[roomcode].admin == uuid) {
                    this.rooms[roomcode].admin = this.rooms[roomcode].users[0]
                }
            }
            return [true, "User left room."]
        } else if (Object.keys(this.games).includes(roomcode)) {
            this.games[roomcode].users = this.games[roomcode].users.filter((id) => {
                return id != uuid
            })
            this.users[uuid].room = null
            if (this.games[roomcode].users.length <= 0) {
                clearTimeout(this.games[roomcode].destroyer)
                delete this.games[roomcode]
            } else {
                if (this.games[roomcode].admin == uuid) {
                    this.games[roomcode].admin = this.games[roomcode].users[0]
                }
            }
            return [true, "User left game."]
        } else {
            this.users[uuid].room = null
            return [false, "Requested room does not exist."]
        }
    }

    joinRoom(uuid, roomcode) {
        if (Object.keys(this.rooms).some(code => {
            return code == roomcode
        })) {
            if (this.rooms[roomcode].users.length < 8) {
                if (this.users[uuid].room && !Object.keys(this.games).includes(this.users[uuid].room)) {
                    return [false, "User already in a room."]
                }
                this.rooms[roomcode].users.push(uuid)
                this.users[uuid].room = roomcode
                return [true, "User joined."]
            } else {
                return [false, "Room full."]
            }
        } else {
            return [false, "Room not found."]
        }
    }

    validateSettings(s) {
        return (3 <= s.regularRounds && s.regularRounds <= 15 &&
            0 <= s.wildcardRounds && s.wildcardRounds <= 4 &&
            10 <= s.roundLength && s.roundLength <= 30 &&
            0.5 <= s.wildcardScoreMultiplier && s.wildcardScoreMultiplier <= 2.0);
    }

    changeRoomSettings(uuid, settingsObj) {
        console.log(settingsObj)
        if (this.users[uuid].room == null) {
            return [false, "User is not in any valid room."]
        }
        let roomcode = this.users[uuid].room
        if (!Object.keys(this.rooms).some(code => {
            return code == roomcode
        })) {
            return [false, "Room not found."]
        } else {
            if (this.rooms[roomcode].admin != uuid) {
                return [false, "User is not admin."]
            } else {
                var propertiesToTest = ["regularRounds",
                                        "wildcardRounds",
                                        "roundLength",
                                        "wildcardScoreMultiplier",
                                        "relativeScoring"];
                if (propertiesToTest.every(function (x) {
                    return x in settingsObj;
                })) {

                    if (this.validateSettings(settingsObj)) {
                        //console.log(this.validateSettings(settingsObj))
                        this.rooms[roomcode].settings = settingsObj;
                        return [true, "Settings updated."]
                    } else {
                        return [false, "Invalid settings."]
                    }
                } else {
                    return [false, "Settings object is not complete."]
                }
            }
        }

    }

    startGame(uuid) {
        if (this.users[uuid].room == null) {
            return [false, "User is not in any valid room."]
        }
        let roomcode = this.users[uuid].room
        if (!Object.keys(this.rooms).some(code => {
            return code == roomcode
        })) {
            return [false, "Room not found."]
        }
        if (this.rooms[roomcode].admin != uuid) {
            return [false, "User is not admin."]
        }
        if (this.rooms[roomcode].users.length < 2 || this.rooms[roomcode].users.length > 8) {
            return [false, "Number of players must be between 2 and 8."]
        }
        this.games[roomcode] = this.rooms[roomcode];
        clearTimeout(this.rooms[roomcode].destroyer)
        delete this.rooms[roomcode];
        this.games[roomcode]["roundsFinished"] = 0
        this.games[roomcode]["gameState"] = GAMESTATE.GAME_START
        var roundlist = []
        for (let i = 0; i < this.games[roomcode].settings.regularRounds; i++) {
            roundlist.push(ROUNDTYPE.REGULAR)
        }
        for (let i = 0; i < this.games[roomcode].settings.wildcardRounds; i++) {
            roundlist.push(ROUNDTYPE.WILDCARD)
        }

        this.games[roomcode]["roundOrder"] = roundlist;
        this.games[roomcode]["rejoinable"] = false;
        var scores = []
        for (let id of this.games[roomcode].users) {
            scores.push({"uuid": id, "score": 0})
        }
        this.games[roomcode]["scores"] = scores
        var answers = []
        for (let id of this.games[roomcode].users) {
            answers.push({"uuid": id, "answerId": -1})
        }
        this.games[roomcode]["currentAnswers"] = answers
        this.games[roomcode]["questionManager"] = new QuestionManager([QuizDBQuestionSource,
                                                                       GPT3QuestionSource]);
        this.games[roomcode]["timeout"] = null
        this.games[roomcode]["lastGameObject"] = {}
        this.games[roomcode]["topics"] = []
        this.games[roomcode]["answered"] = {"topic": []}
        this.games[roomcode]["answers"] = {}
        this.games[roomcode]["lastQuestionTime"] = null
        this.games[roomcode]["correctId"] = null
        for (let i = 0; i < this.games[roomcode].roundOrder.length; i++) {
            this.games[roomcode]["answered"][i] = []
        }
        setTimeout(this.propagateGame.bind(this), 150, roomcode)
        return [true, "Game started."]
    }

    getLastGameInfo(uuid) {
        if (this.users[uuid].room == null) {
            return [false, "User is not in any valid game."]
        }
        let gameid = this.users[uuid].room
        if (!Object.keys(this.games).some(code => {
            return code == gameid
        })) {
            return [false, "Game not found."]
        }
        if (this.games[gameid].lastGameObject.questionObj != null) {
            let time = Date.now()
            this.games[gameid].lastGameObject.questionObj.remainingTime -= (time - this.games[gameid].lastQuestionTime) / 1000.0;
            this.games[gameid].lastQuestionTime = time
        }
        return [true, this.games[gameid].lastGameObject]

    }

    getUsersByGameId(gameid) {
        if (!Object.keys(this.games).some(code => {
            return code == gameid
        })) {
            return [false, "Game not found."]
        }
        return [true, this.games[gameid].users]
    }

    saveTopics(uuid, topics) {
        if (this.users[uuid].room == null) {
            return [false, "User is not in any valid game."]
        }
        let gameid = this.users[uuid].room
        if (!Object.keys(this.games).some(code => {
            return code == gameid
        })) {
            return [false, "Game not found."]
        }
        if (this.games[gameid].answered.topic.includes(uuid)) {
            return [false, "Topics already recorded."]
        }
        if (this.games[gameid].roundsFinished != 0 || this.games[gameid].lastGameObject.gameState != GAMESTATE.TOPIC) {
            return [false, "Topic sent too late."]
        }
        if (topics.length != 2) {
            return [false, "Exactly 2 topics are expected."]
        }

        for (let topic of topics) {
            if (topic != null) {
                this.games[gameid].topics.push(topic)
            }
        }
        this.games[gameid].answered.topic.push(uuid)
        var gameInfoObj = {
            gameState: GAMESTATE.WAIT,
            message: "Topics recorded, waiting for other players...",
            roundType: null,
            questionObj: null,
            leaderBoard: this.createLeaderBoard(this.games[gameid]),
            rejoinable: false,
            roomCode: gameid
        }
        if (this.games[gameid].answered.topic.length >= this.games[gameid].users.length) {
            if (this.games[gameid].timeout != null) {
                clearTimeout(this.games[gameid].timeout)
                this.games[gameid].timeout = setTimeout(
                    this.propagateGame.bind(this),
                    500,
                    gameid
                )
            }
        }
        return [true, gameInfoObj]

    }


    removeUser(uuid) {
        if (Object.keys(this.users).includes(uuid)) {
            delete this.users[uuid]
        }
    }

    saveAnswer(uuid, answer) {
        if (this.users[uuid].room == null) {
            return [false, "User is not in any valid game."]
        }
        let gameid = this.users[uuid].room
        if (!Object.keys(this.games).some(code => {
            return code == gameid
        })) {
            return [false, "Game not found."]
        }
        if (this.games[gameid].answered[this.games[gameid].roundsFinished - 1].includes(
            uuid)) {
            return [false, "Answer already recorded."]
        }
        if (this.games[gameid].lastGameObject.gameState != GAMESTATE.QUESTION) {
            return [false, "Answer sent too late."]
        }
        if (answer > 4 || answer < 0) {
            return [false, "Answer ID invalid."]
        }

        this.games[gameid].answers[uuid] = answer

        this.games[gameid].answered[this.games[gameid].roundsFinished - 1].push(uuid)
        var gameInfoObj = {
            gameState: GAMESTATE.WAIT,
            message: "Answer recorded, waiting for other players...",
            roundType: null,
            questionObj: null,
            leaderBoard: this.createLeaderBoard(this.games[gameid]),
            rejoinable: false,
            roomCode: gameid
        }
        if (this.games[gameid].answered[this.games[gameid].roundsFinished - 1].length >= this.games[gameid].users.length) {
            if (this.games[gameid].timeout != null) {
                clearTimeout(this.games[gameid].timeout)
                this.games[gameid].timeout = setTimeout(
                    this.propagateGame.bind(this),
                    5,
                    gameid
                )
            }
        }
        return [true, gameInfoObj]

    }

    handleGameStart(g, gameid) {
        for (let i = 0; i < g.settings.regularRounds; i++) {
            g.questionManager.scheduleQuestion(QuizDBQuestionSource.type, {})
        }
        var gameInfoObj = {
            gameState: GAMESTATE.GAME_START,
            message: "The AI mastermind is booting up...",
            roundType: null,
            questionObj: null,
            leaderBoard: this.createLeaderBoard(g),
            rejoinable: false,
            roomCode: gameid
        }
        this.reactor.dispatchManually("GameInfoUpdateRequest", gameInfoObj)
        g.lastGameObject = gameInfoObj
        g.timeout = setTimeout(this.propagateGame.bind(this), 2000, gameid)
        if(g.settings.wildcardRounds==0){
            g.gameState = GAMESTATE.WAIT
        }
        else {
            g.gameState = GAMESTATE.TOPIC
        }
    }

    handleTopic(g, gameid) {
        var gameInfoObj = {
            gameState: GAMESTATE.TOPIC,
            message: "Please enter two topics for the wildcard rounds! (max 2 words, 32 characters).",
            roundType: null,
            questionObj: null,
            leaderBoard: this.createLeaderBoard(g),
            rejoinable: false,
            roomCode: gameid
        }
        this.reactor.dispatchManually("GameInfoUpdateRequest", gameInfoObj)
        g.timeout = setTimeout(this.propagateGame.bind(this), 45000, gameid)
        g.gameState = GAMESTATE.WAIT
        g.lastGameObject = gameInfoObj
    }

    handleWait(g, gameid) {
        if (g.roundsFinished == 0) {
            if(g.settings.wildcardRounds>0) {
                let selectedTopics = []
                if (g.topics.length == 0) {
                    for (let i = 0; i < g.settings.wildcardRounds; i++) {
                        selectedTopics.push("information technology")
                    }
                } else if (g.topics.length < g.settings.wildcardRounds) {
                    for (let topic of g.topics) {
                        selectedTopics.push(topic)
                    }
                    for (let i = 0; i < (g.settings.wildcardRounds - g.topics.length); i++) {
                        selectedTopics.push(shuffleArray(g.topics)[0])
                    }
                } else {
                    g.topics = shuffleArray(g.topics)
                    for (let i = 0; i < g.settings.wildcardRounds; i++) {
                        selectedTopics.push(g.topics[i])
                    }
                }
                for (let topic of selectedTopics) {
                    g.questionManager.scheduleQuestion(
                        GPT3QuestionSource.type,
                        {topic: topic}
                    )
                }
            }

            var gameInfoObj = {
                gameState: GAMESTATE.WAIT,
                message: "Generating questions based on topics...",
                roundType: null,
                questionObj: null,
                leaderBoard: this.createLeaderBoard(g),
                rejoinable: false,
                roomCode: gameid
            }
            this.reactor.dispatchManually("GameInfoUpdateRequest", gameInfoObj)
            g.timeout = setTimeout(this.propagateGame.bind(this), WAITTIME, gameid)
            g.gameState = GAMESTATE.ROUND_START
            g.lastGameObject = gameInfoObj
        } else if (g.roundsFinished < g.roundOrder.length) {
            this.calculateScore(g)
            let questionObj = g.lastGameObject.questionObj
            questionObj.correctId = g.correctId
            var gameInfoObj = {
                gameState: GAMESTATE.WAIT,
                message: "Next round incoming...",
                roundType: null,
                questionObj: questionObj,
                leaderBoard: this.createLeaderBoard(g),
                rejoinable: false,
                roomCode: gameid
            }
            this.reactor.dispatchManually("GameInfoUpdateRequest", gameInfoObj)
            g.timeout = setTimeout(this.propagateGame.bind(this), WAITTIME, gameid)
            g.gameState = GAMESTATE.ROUND_START
            g.lastGameObject = gameInfoObj
        } else if (g.roundsFinished == g.roundOrder.length) {
            this.calculateScore(g)
            let questionObj = g.lastGameObject.questionObj
            questionObj.correctId = g.correctId
            var gameInfoObj = {
                gameState: GAMESTATE.WAIT,
                message: "Extremely complex leaderboard calculations in progress...",
                roundType: null,
                questionObj: questionObj,
                leaderBoard: this.createLeaderBoard(g),
                rejoinable: false,
                roomCode: gameid
            }
            this.reactor.dispatchManually("GameInfoUpdateRequest", gameInfoObj)
            g.timeout = setTimeout(this.propagateGame.bind(this), WAITTIME, gameid)
            g.gameState = GAMESTATE.GAME_END
            g.lastGameObject = gameInfoObj
        }
    }

    handleRoundStart(g, gameid) {
        let roundType = g.roundOrder[g.roundsFinished]
        let errmsg = ""
        if (roundType == ROUNDTYPE.REGULAR) {
            while (!g.questionManager.checkReady(QuizDBQuestionSource.type)) {
                //sleep(100)
            }
        } else if (roundType == ROUNDTYPE.WILDCARD) {
            if (!g.questionManager.checkReady(GPT3QuestionSource.type)) {
                roundType = ROUNDTYPE.REGULAR
                errmsg = "\nError 404: AI is at the fridge snacking on shredded cheese.\nFalling back to regular round!"
                g.questionManager.scheduleQuestion(QuizDBQuestionSource.type, {})
                while (!g.questionManager.checkReady(QuizDBQuestionSource.type)) {
                    //sleep(100)
                }
            }
        }
        var gameInfoObj = {
            gameState: GAMESTATE.ROUND_START,
            message: "Round " + (g.roundsFinished + 1) + errmsg,
            roundType: roundType,
            questionObj: null,
            leaderBoard: this.createLeaderBoard(g),
            rejoinable: false,
            roomCode: gameid
        }
        this.reactor.dispatchManually("GameInfoUpdateRequest", gameInfoObj)
        g.timeout = setTimeout(this.propagateGame.bind(this), WAITTIME, gameid)
        g.gameState = GAMESTATE.QUESTION
        g.lastGameObject = gameInfoObj
    }

    handleQuestion(g, gameid) {
        if (g.lastGameObject.roundType == ROUNDTYPE.REGULAR) {
            var qtype = QuizDBQuestionSource.type
        } else if (g.lastGameObject.roundType == ROUNDTYPE.WILDCARD) {
            var qtype = GPT3QuestionSource.type
        }
        let question = g.questionManager.getQuestion(qtype)
        let questionObj = {
            question: question.question,
            answers: question.answers,
            remainingTime: g.settings.roundLength
        }
        g.correctId = question.correctId
        console.log("Correct answer: "+questionObj.answers[g.correctId])
        var gameInfoObj = {
            gameState: GAMESTATE.QUESTION,
            message: "Question " + (g.roundsFinished + 1),
            roundType: g.lastGameObject.roundType,
            questionObj: questionObj,
            leaderBoard: this.createLeaderBoard(g),
            rejoinable: false,
            roomCode: gameid
        }
        this.reactor.dispatchManually("GameInfoUpdateRequest", gameInfoObj)
        g.timeout = setTimeout(
            this.propagateGame.bind(this),
            g.settings.roundLength * 1000 + OVERHEADPADDINGTIME,
            gameid
        )
        g.gameState = GAMESTATE.WAIT
        g.lastQuestionTime = Date.now();
        g.lastGameObject = gameInfoObj
        g.roundsFinished += 1
    }

    handleGameEnd(g, gameid) {
        var gameInfoObj = {
            gameState: GAMESTATE.GAME_END,
            message: "Game Over!",
            roundType: g.lastGameObject.roundType,
            questionObj: g.lastGameObject.questionObj,
            leaderBoard: this.createLeaderBoard(g),
            rejoinable: false,
            roomCode: gameid
        }
        this.reactor.dispatchManually("GameInfoUpdateRequest", gameInfoObj)
        g.lastGameObject = gameInfoObj
        gameInfoObj.rejoinable = true
        setTimeout(this.destroyGame.bind(this), 120000, gameid)

        this.reactor.dispatchManually(
            "PersonalGameInfoUpdateRequest",
            {
                response: gameInfoObj,
                clients: [g.admin,]
            }
        )

    }

    calculateScore(g) {
        let corrects = []
        for (let user of g.users) {
            if (Object.keys(g.answers).includes(user)) {
                if (g.answers[user] == g.correctId) {
                    corrects.push(user)
                }
            }
        }
        if (corrects.length <= 0) {
            return
        }
        if (g.roundOrder[g.roundsFinished - 1] == ROUNDTYPE.REGULAR) {
            var scoreMultiplier = 1.0
        } else if (g.roundOrder[g.roundsFinished - 1] == ROUNDTYPE.WILDCARD) {
            var scoreMultiplier = g.settings.wildcardScoreMultiplier
        }

        if (g.settings.relativeScoring) {
            scoreMultiplier /= corrects.length
        }

        for (let id of corrects) {
            g.scores.find(x => x.uuid == id).score += Math.round(1000 * scoreMultiplier)
        }

        g.scores.sort((a, b) => {
            return b.score - a.score;
        })
    }

    destroyGame(gameid) {
        if(Object.keys(this.games).includes(gameid)) {
            for (let user of this.games[gameid].users) {
                try {
                    this.users[user].room = null
                }
                catch (e){

                }
            }
            delete this.games[gameid]
        }
    }

    destroyRoom(roomid) {
        if(Object.keys(this.rooms).includes(roomid)){
        for (let user of this.rooms[roomid].users) {
            try {
                this.users[user].room = null
            }
            catch (e) {

            }
        }
            delete this.rooms[roomid]
        }
    }

    propagateGame(gameid) {
        if (!Object.keys(this.games).some(code => {
            return code == gameid
        })) {
            return [false, "Game not found."]
        }

        let g = this.games[gameid]
        switch (g.gameState) {
            case GAMESTATE.GAME_START:
                this.handleGameStart(g, gameid)
                break;

            case GAMESTATE.TOPIC:
                this.handleTopic(g, gameid)
                break;

            case GAMESTATE.WAIT:
                this.handleWait(g, gameid)
                break;

            case GAMESTATE.ROUND_START:
                this.handleRoundStart(g, gameid)
                break;

            case GAMESTATE.QUESTION:
                this.handleQuestion(g, gameid)
                break;

            case GAMESTATE.GAME_END:
                this.handleGameEnd(g, gameid)
                break;

            default:
                break;
        }

    }


}

export const Engine = new TriviAIEngine();

/*exports
    .Engine = Engine
exports
    .GAMESTATE = GAMESTATE
exports
    .ROUNDTYPE = ROUNDTYPE
*/
