import {shuffleArray} from "../util.js"

export class QuestionManager {
    constructor(sourcelist) {
        this.sources = {}
        for (let source of sourcelist) {
            this.sources[source.type] = source
        }
        this.questions = {}
        for (let source of sourcelist) {
            this.questions[source.type] = []
        }
    }

    scheduleQuestion(sourceType, data) {
        this.questions[sourceType].push(this.sources[sourceType].generateQuestion(data))
    }

    checkReady(sourceType) {
        this.questions[sourceType].sort((a, b) => {
            if (a.getData() == null && b.getData() != null) {
                return 1
            } else if (a.getData() != null && b.getData() == null) {
                return -1
            } else {
                return 0
            }
        })
        return this.questions[sourceType][0].getData() != null
    }

    getQuestion(sourceType) {
        let questionObj = this.questions[sourceType][0].getData()
        this.questions[sourceType].shift()
        if (questionObj == null) {
            return null
        }
        let correctAnswer = questionObj.answers[questionObj.correctId]
        var answers = shuffleArray(questionObj.answers)
        var correctId = answers.indexOf(correctAnswer)

        return {
            question: questionObj.question,
            answers: answers,
            correctId: correctId
        }
    }

}

// exports.QuestionManager = QuestionManager;
