import {EventReactor} from "./CommunicationEventHandler/EventReactor.js"
import {
    RegisterClientEventHandler,
    CreateRoomEventHandler,
    LeaveRoomEventHandler,
    JoinRoomEventHandler,
    UpdateRoomSettingsEventHandler,
    StartGameEventHandler,
    GameInfoUpdateRequestEventHandler,
    SendTopicEventHandler,
    SendAnswerEventHandler,
    PersonalGameInfoUpdateRequestEventHandler,
    ConnectionLostEventHandler
} from "./CommunicationEventHandler/Handlers.js"
import {Engine} from "./GameEngine/TriviAIEngine.js"


const HOST = process.env.SERVERHOST || 'ws://localhost';
const PORT = process.env.SERVERPORT || 8080;

console.log("Launching TriviAI server at " + HOST + ":" + PORT + "")
const reactor = new EventReactor(HOST, PORT);
Engine.addReactor(reactor);
reactor.registerHandler(RegisterClientEventHandler);
reactor.registerHandler(CreateRoomEventHandler);
reactor.registerHandler(LeaveRoomEventHandler);
reactor.registerHandler(JoinRoomEventHandler);
reactor.registerHandler(UpdateRoomSettingsEventHandler);
reactor.registerHandler(StartGameEventHandler);
reactor.registerHandler(GameInfoUpdateRequestEventHandler);
reactor.registerHandler(SendTopicEventHandler);
reactor.registerHandler(SendAnswerEventHandler);
reactor.registerHandler(PersonalGameInfoUpdateRequestEventHandler);
reactor.registerHandler(ConnectionLostEventHandler);

